*Public commands*
These commands can be used by anyone\.

/address /ip
Get the address that can be used to connect to the server\.

/status
Get a short description of the servers status\.

/identify /id
Get the id of the sender and the chat\. These can be used in settings\.conf\.

/help
Show this message\.

*Admin commands*
These commands can only be used by admins defined in settings\.conf\.

/startServer /st
Start the server\.

/stopServer /sp
Stop the server\.

/save /sv
Save the world\. Kind of useless but lets you force a save if you really want to\. 

/run /command /exec
Input any command into the server console\. List of available commands can be found [here](https://minecraft\.fandom\.com/wiki/Commands#List_and_summary_of_commands)\. You can also use any commands from mods you have installed\.

Example: `/exec kill @a`

/say
Send a message to the server chat\. Players will see your telegram username\.

Example: `/say Hello from server admin!`

Message in chat: `[Server] Telergram admin @tgadminusername: Hello from server admin!`

/getLog /log
Get recent server logs\. Sent as a file if too long\. You can specify how many lines you want\. Maximum is 1000\.

Example: `/log 50`

/listBackups /ls
Get a list of local and remote backups\.

/createBackup /backup /cb
Create a new local backup\. You can specify a tag to help identify the backup\.

Example: `/cb`

Example: `/cb my tag`

The following 3 commands work in the same way\. Just switch the command\.

/backupStart /bs
Create a new local backup and start the server\. You can specify a tag to help identify the backup\.

/backupUpload /bu
Create a new local backup and upload it\. You can specify a tag to help identify the backup\.

/backupStartUpload /bsu
Create a new local backup, start the server and upload the backup\. You can specify a tag to help identify the backup\.

/loadBackup /load /lb
Load a local backup\. *THIS WILL OVERWRITE THE CURRENT WORLD*\. Make a backup first if you don't want to lose it\. You need to specify the filename of the backup you want to load\. Get a list by using /ls\.

Example: `/load world\-13\.10\.2023\-18\.43\.51\-test\.tar\.xz`

/loadStart /lbs
Load a local backup and start the server\. *THIS WILL OVERWRITE THE CURRENT WORLD*\. Make a backup first if you don't want to lose it\. You need to specify the filename of the backup you want to load\. Get a list by using /ls\.

Example: `/loadStart world\-13\.10\.2023\-18\.43\.51\-test\.tar\.xz`

/removeLocal /rl
Remove a local backup\. *THIS WILL PERMANENTLY REMOVE IT*\. You need to specify the filename of the backup you want to remove\. Get a list by using /ls\.

Example: `/rl world\-13\.10\.2023\-18\.43\.51\-test\.tar\.xz`

/removeRemote /rr
Remove a remote backup\. *THIS WILL PERMANENTLY REMOVE IT*\. You need to specify the filename of the backup you want to remove\. Get a list by using /ls\.

Example: `/rr world\-13\.10\.2023\-18\.43\.51\-test\.tar\.xz`

/downloadBackup /download /dl
Download a remote backup to the local folder\. You need to specify the filename of the backup you want to download\. Get a list by using /ls\.

Example: `/dl world\-13\.10\.2023\-18\.43\.51\-test\.tar\.xz`

/uploadBackup /upload /ul
Upload a local backup to the remote\. You need to specify the filename of the backup you want to upload\. Get a list by using /ls\.

Example: `/ul world\-13\.10\.2023\-18\.43\.51\-test\.tar\.xz`
