# A telegram bot for Minecraft server admins for managing the server remotely
### Made as a school project on top of the provided base.

## Usage
All available commands are listed in [help.md](help.md). Type /help once you have the bot running to list the same commands.

## Platforms

- Linux: Tested on Endeavour OS and Ubuntu Server.
- Windows: Probably won't work, not tested.
- mac: Might work, not tested. Default settings might need to be changed.


## rclone

rclone can be used for storing backups in the cloud. To use install rclone and configure a remote. If running the bot as root, make sure rclone is configured for root. Tested with OneDrive as remote.


## Settings

While the bot can take into account user error, the settings need to be configured correctly or unexpected behavior or unhanled errors may occur.
All paths must be absolute.

### General

#### runCommand
- Executed in serverPath. The default should be fine. If a non default value is causing problems save the command is a file called run.sh inside the server folder and set this to "./run.sh". Don't forget to make the script executable.
#### serverPath
- Path to the directory containing server.jar and other server files.
####  domain
- If you use a domain to connect to the server set it here.
#### srv
- Set this to true if you are using a domain and have a srv record to resolve non default port.
#### tempCommand
- The command for getting temperatures. If you only want cpu temp change the * after thermal_zone to the correct number for your system.
#### loadCommand
- The command for getting cpu load. Default should be fine.
#### ramCommand
- The command for getting ram use. Default should be fine.

### Backups

#### backupPath
- Path to the directory you want to store backups in. Must only contain backups created by this program.
#### compressCommand
- Command for creating backup archive. -T controls how many threads are used. The default 0 uses all. Remove to use single threaded mode. The %s will always get formatted by backupPath, worldPath in that order.
#### decompressCommand
- Command for extracting backup archive. The %s will always get formatted by backupPath, worldPath in that order.
### localStorageUseCommand
- Command for getting disk usage for the disk that local backups are stored on. The first %s will get replaced by backupPath.
### remoteStorageUseCommand
- Command for getting remote storage usage for the rclone remote. The first %s will get replaced by remote.

### rclone

#### remote
- Path to the rclone remote. For example: "my-remote:backups". This directory must exist. Must only contain backups created by this program. Leave empty to disable remote backups.
#### args
- Argument to be passed to rclone. Use to set bandwidth limit for example.

### Telegram

#### adminUserIDs
- List of telegram user IDs authorized to use admin commands. Use /identify to get your id.
#### logBroadcastSubscriberChatIDs
- List of telegram chat IDs subscribed to the chat log. Use /identify to get your id. To get a channel id open a web client, locate the channel and look at the url.

## Server setup

- Get the server jar. Must be 1.7 or newer.
- Run once and accept the EULA.
- Run again to generate rest of the config files.


## Running on headless server

- [Build a jar](https://www.baeldung.com/scala/sbt-fat-jar).
- Place the jar, [help.md](help.md) and [settings.conf](settings.conf) in the same directory.
- Edit [settings.conf](settings.conf) to match your preferences.
- Create bot_token.txt containing your telegram bot token in the same directory.
- Run the jar.
  - Make sure your java runtime can support the same class file versions as the java runtime you use to compile.
    - Compile for java 17 if this is causing issues.
- Can be auto-started at startup by using a systemd service for example.


## Troubleshooting

- Try running the bot as root. Make sure to configure rclone for root.
- Make sure you are using the correct java version for the bot and server. They might not be the same.


## Plan

Re-implement https://gitlab.com/JUMBUZI/mc-server-manager-discrord-linux and its submodule as a Telegram bot in Scala while fixing some of the originals problems.
