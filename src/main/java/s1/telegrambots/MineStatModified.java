package s1.telegrambots;

import com.google.gson.*;
import java.io.*;
import java.net.*;
import java.util.*;

import java.util.stream.IntStream;

/*
 * MineStat.java - A Minecraft server status checker, modified to include player list
 * Copyright (C) 2014-2022 Lloyd Dilley
 * http://www.dilley.me/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @author Lloyd Dilley, modified by Riku Väättänen
 */
public class MineStatModified {

    public static final String VERSION = "3.0.5";         // MineStat version
    public static final byte NUM_FIELDS = 6;              // number of values expected from server
    public static final int DEFAULT_TIMEOUT = 5;          // default TCP/UDP timeout in seconds
    public static final int DEFAULT_SLP_PORT = 25565;     // default TCP port

    public enum ConnectionStatus
    {
        /** The connection was successful and the response data was parsed without problems. */
        SUCCESS(0, "Success"),

        /** The connection failed due to an unknown hostname or incorrect port number. */
        CONNFAIL(-1, "Connection Failure"),

        /** The connection timed out. Either the server is overloaded or it dropped our packets. */
        TIMEOUT(-2, "Timeout"),

        /** The connection was successful, but the response data could not be properly parsed. */
        UNKNOWN(-3, "Unknown");

        private final int magicNumber;
        private final String shortDescription;

        ConnectionStatus(int magicNumber, String shortDescription) {
            this.magicNumber = magicNumber;
            this.shortDescription = shortDescription;
        }

        public int getMagicNumber() { return magicNumber; }

        /**
         * Returns a human-readable short description of the ConnectionStatus.
         * @return A short human-readable description.
         **/
        @Override
        public String toString() { return shortDescription; }
    }

    /**
     * Hostname or IP address of the Minecraft server
     */
    private String address;

    /**
     * Port number the Minecraft server accepts connections on
     */
    private int port;

    /**
     * TCP socket connection timeout in seconds
     */
    private int timeout;

    /**
     * Is the server up? (true or false)
     */
    private boolean serverUp;

    /**
     * Game mode
     * @since 3.0.0
     */
    private String gameMode;

    /**
     * Message of the day from the server
     */
    private String motd;

    /**
     * Message of the day from the server,
     * without any formatting (human-readable)
     * @since 2.1.0
     */
    private String strippedMotd;

    /**
     * Minecraft version the server is running
     */
    private String version;

    /**
     * Current number of players on the server
     */
    private int currentPlayers;

    /**
     * Maximum player capacity of the server
     */
    private int maximumPlayers;

    /**
     * Base64-encoded favicon possibly contained in JSON 1.7 responses
     * @since 3.0.2
     */
    private String faviconB64;

    /**
     * Decoded favicon data
     * @since 3.0.2
     */
    private String favicon;

    /**
     * Ping time to server in milliseconds
     */
    private long latency;

    /**
     * Protocol level
     * Note: Multiple Minecraft versions can share the same protocol level
     * @since 3.0.0
     */
    private int protocol;

    /**
     * All data as JsonObject
     */
    private JsonObject json;

    private List<String> playerList;

    /**
     * Connection status
     * @since 3.0.2
     */
    private final MineStatModified.ConnectionStatus connectionStatus;

    public MineStatModified(String address)
    {
        this(address, DEFAULT_SLP_PORT, DEFAULT_TIMEOUT, false);
    }

    public MineStatModified(String address, int port)
    {
        this(address, port, DEFAULT_TIMEOUT, true);
    }

    public MineStatModified(String address, int port, int timeout)
    {
        this(address, port, timeout, true);
    }

    public MineStatModified(String address, int port, int timeout, boolean isPortDefined)
    {
        setAddress(address);
        setPort(port);
        setTimeout(timeout);
        MineStatModified.ConnectionStatus intConnectionStatus = MineStatModified.ConnectionStatus.UNKNOWN;

        intConnectionStatus = jsonRequest(address, port, getTimeout());

        if(isServerUp())
            this.connectionStatus = MineStatModified.ConnectionStatus.SUCCESS;
        else
            this.connectionStatus = intConnectionStatus;
    }

    public String getAddress() { return address; }

    private void setAddress(String address) { this.address = address; }

    public int getPort() { return port; }

    private void setPort(int port) { this.port = port; }

    public int getTimeout() { return timeout * 1000; } // convert to milliseconds

    private void setTimeout(int timeout) { this.timeout = timeout; }

    public String getGameMode() { return gameMode; }

    private void setGameMode(String gameMode) { this.gameMode = gameMode; }

    public String getMotd() { return motd; }

    private void setMotd(String motd) { this.motd = motd; }

    public String getStrippedMotd() {
        return strippedMotd;
    }

    private void setStrippedMotd(String strippedMotd) {
        this.strippedMotd = strippedMotd;
    }

    /**
     * Helper function for stripping any formatting from a motd.
     * @param motd A motd with formatting codes
     * @return A motd with all formatting codes removed
     * @since 2.1.0
     */
    public String stripMotdFormatting(String motd) {
        return motd.replaceAll("§.", "");
    }

    public String stripMotdFormatting(JsonObject motd) {
        StringBuilder strippedMotd = new StringBuilder();

        if(motd.isJsonPrimitive()) {
            return motd.getAsString();
        }

        JsonObject motdObj = motd.getAsJsonObject();
        if(motdObj.has("text")) {
            strippedMotd.append(motdObj.get("text").getAsString());
        }

        if(motdObj.has("extra") && motdObj.get("extra").isJsonArray()) {
            for(JsonElement extraElem : motdObj.get("extra").getAsJsonArray()) {
                strippedMotd.append(stripMotdFormatting(extraElem.getAsJsonObject()));
            }
        }

        return strippedMotd.toString();
    }

    public String getVersion() { return version; }

    private void setVersion(String version) { this.version = version; }

    public int getCurrentPlayers() { return currentPlayers; }

    private void setCurrentPlayers(int currentPlayers) { this.currentPlayers = currentPlayers; }

    public int getMaximumPlayers() { return maximumPlayers; }

    private void setMaximumPlayers(int maximumPlayers) { this.maximumPlayers = maximumPlayers; }

    public String getFaviconB64() { return faviconB64; }

    private void setFaviconB64(String faviconB64) { this.faviconB64 = faviconB64; }

    public String getFavicon() { return favicon; }

    private void setFavicon(String favicon) { this.favicon = favicon; }

    public long getLatency() { return latency; }

    private void setLatency(long latency) { this.latency = latency; }

    public int getProtocol() { return protocol; }

    private void setProtocol(int protocol) { this.protocol = protocol; }

    public boolean isServerUp() { return serverUp; }

    public JsonObject getJson() { return this.json; }

    private void setJson(JsonObject json) { this.json = json; }

    public List<String> getPlayerList() { return this.playerList; }

    private void setPlayerList(List<String> playerList) { this.playerList = playerList; }

    public MineStatModified.ConnectionStatus getConnectionStatus() { return connectionStatus; }
    public String getConnectionStatusDescription() { return connectionStatus.toString(); }

    /*
     * Unpack an int from a varint
     */
    public int recvVarInt(DataInputStream dis)
    {
        try
        {
            int intData = 0, width = 0;
            while(true)
            {
                int varInt = dis.readByte();
                intData |= (varInt & 0x7F) << width++ * 7;
                if(width > 5)
                    return MineStatModified.ConnectionStatus.UNKNOWN.getMagicNumber(); // overflow
                if((varInt & 0x80) != 128)           // Little Endian Base 128 (LEB128)
                    break;
            }
            return intData;
        }
        catch(IOException ioe)
        {
            return MineStatModified.ConnectionStatus.UNKNOWN.getMagicNumber();
        }
    }

    /*
     * Pack a varint from an int
     */
    public void sendVarInt(DataOutputStream dos, int intData)
    {
        try
        {
            while(true)
            {
                if((intData & 0xFFFFFF80) == 0)
                {
                    dos.writeByte(intData);
                    return;
                }
                dos.writeByte(intData & 0x7F | 0x80);
                intData >>>= 7;
            }
        }
        catch(IOException ignored)
        {
        }
    }

    /*
     * Check if MineStat object data is present
     */
    public boolean isDataValid()
    {
        // Do not check for empty motd in case server has none
        return this.motd != null && this.version != null && !this.version.trim().isEmpty() && currentPlayers >= 0 && maximumPlayers >= 0;
    }

    /*
     * 1.7
     * 1.7 to current servers communicate as follows for a ping request:
     * 1. Client sends:
     *   1a. \x00 (handshake packet containing the fields specified below)
     *   1b. \x00 (request)
     * The handshake packet contains the following fields respectively:
     *   1. protocol version as a varint (\x00 suffices)
     *   2. remote address as a string
     *   3. remote port as an unsigned short
     *   4. state as a varint (should be 1 for status)
     * 2. Server responds with:
     *   2a. \x00 (JSON response)
     * An example JSON string contains:
     *   {'players': {'max': 20, 'online': 0},
     *   'version': {'protocol': 404, 'name': '1.13.2'},
     *   'description': {'text': 'A Minecraft Server'}}
     */
    public MineStatModified.ConnectionStatus jsonRequest(String address, int port, int timeout)
    {
        try
        {
            Socket clientSocket = new Socket();
            long startTime = System.currentTimeMillis();
            clientSocket.connect(new InetSocketAddress(getAddress(), getPort()), getTimeout());
            setLatency(System.currentTimeMillis() - startTime);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream payload = new DataOutputStream(baos);
            DataOutputStream dos = new DataOutputStream(clientSocket.getOutputStream());
            DataInputStream dis = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
            payload.writeByte(0x00);               // handshake packet
            sendVarInt(payload, 0x00);             // protocol version
            sendVarInt(payload, address.length()); // packed remote address length as varint
            payload.writeBytes(address);           // remote address as string
            payload.writeShort(port);              // remote port as short
            sendVarInt(payload, 0x01);             // state packet
            sendVarInt(dos, baos.size());          // payload size as varint
            dos.write(baos.toByteArray());         // send payload
            dos.writeByte(0x01);                   // size
            dos.writeByte(0x00);                   // ping packet
            recvVarInt(dis);
            recvVarInt(dis);
            int jsonLength = recvVarInt(dis);      // JSON response size
            byte[] rawData = new byte[jsonLength]; // storage for JSON data

            dis.readFully(rawData);                // fill byte array with JSON data

            // Close socket
            if(!clientSocket.isClosed())
                clientSocket.close();
            // Populate object from JSON data
            JsonObject jobj = new Gson().fromJson(new String(rawData), JsonObject.class);
            setJson(jobj);

            if (jobj.get("players").getAsJsonObject().has("sample")) {
                JsonArray jsonArray = jobj.get("players").getAsJsonObject().get("sample").getAsJsonArray();

                List<JsonObject> list = new ArrayList<>();

                IntStream.range(0, jsonArray.size()).forEachOrdered(n -> list.add(jsonArray.get(n).getAsJsonObject()));

                setPlayerList(list.stream().map(x -> x.get("name").getAsString()).toList());
            } else {
                setPlayerList(new ArrayList<>());
            }

            setProtocol(jobj.get("version").getAsJsonObject().get("protocol").getAsInt());
            setMotd(jobj.get("description").toString());
            try
            {
                setStrippedMotd(stripMotdFormatting(jobj.get("description").getAsJsonObject()));
            }
            catch(Exception e)
            {
                setStrippedMotd(stripMotdFormatting(jobj.get("description").toString()));
            }
            setVersion(jobj.get("version").getAsJsonObject().get("name").getAsString());
            setCurrentPlayers(jobj.get("players").getAsJsonObject().get("online").getAsInt());
            setMaximumPlayers(jobj.get("players").getAsJsonObject().get("max").getAsInt());
            try
            {
                setFaviconB64(jobj.get("favicon").getAsString().split("base64,")[1]);
                if(getFaviconB64() != null && !getFaviconB64().isEmpty())
                    setFavicon(new String(Base64.getDecoder().decode(getFaviconB64())));
            }
            catch(Exception e)
            {
                setFaviconB64(null);
                setFavicon("");
            }
            serverUp = true;
            setGameMode("Unspecified");
            if(!isDataValid())
                return MineStatModified.ConnectionStatus.UNKNOWN;
        }
        catch(SocketTimeoutException ste)
        {
            return MineStatModified.ConnectionStatus.TIMEOUT;
        }
        catch(EOFException eofe)
        {
            return MineStatModified.ConnectionStatus.UNKNOWN;
        }
        catch(IOException ioe)
        {
            return ConnectionStatus.CONNFAIL;
        }
        return MineStatModified.ConnectionStatus.SUCCESS;
    }

}
