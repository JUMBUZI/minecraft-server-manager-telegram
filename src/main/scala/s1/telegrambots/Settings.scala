package s1.telegrambots

import java.io.File
import com.typesafe.config.ConfigFactory


object Settings:
    private val config = ConfigFactory.parseFile(File((os.pwd / "settings.conf").toString))

    val runCommand = config.getString("runCommand")
    val serverPath = config.getString("serverPath")

    private val serverProperties = ConfigFactory.parseFile(File((os.Path(Settings.serverPath) / "server.properties").toString))

    val worldPath = (os.Path(Settings.serverPath) / serverProperties.getString("level-name")).toString
    val port = serverProperties.getInt("server-port")
    val domain = config.getString("domain")
    val srv = config.getBoolean("srv")
    val tempCommand = config.getString("tempCommand")
    val loadCommand = config.getString("loadCommand")
    val ramCommand = config.getString("ramCommand")

    val backupPath = config.getString("backupPath")
    val compressCommand = config.getString("compressCommand")
    val decompressCommand = config.getString("decompressCommand")

    val remote = config.getString("remote")
    val args = config.getString("args")
    val localStorageUseCommand = config.getString("localStorageUseCommand")
    val remoteStorageUseCommand = config.getString("remoteStorageUseCommand")

    val adminUserIDs = config.getLongList("adminUserIDs")
    val logBroadcastSubscriberChatIDs = config.getLongList("logBroadcastSubscriberChatIDs")
