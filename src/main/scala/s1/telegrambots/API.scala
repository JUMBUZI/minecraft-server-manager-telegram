package s1.telegrambots

import com.google.gson.*

import java.io.FileNotFoundException
import java.nio.file.FileAlreadyExistsException
import java.text.SimpleDateFormat
import scala.collection.mutable.Buffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.math.*
import scala.math.Integral.Implicits.*
import scala.util.control.Breaks.*
import scala.util.{Failure, Success}
import scala.sys.process.*
import scala.collection.immutable.LazyList


class API(var outBufferSize: Int = 100):

    private var process: os.SubProcess = _
    private var inBuffer = Buffer[String]()
    private var startTime: Long = 0
    var outBuffer = Buffer[String]()
    var started = false
    var crashed = false
    var backupRunning = false
    var backupTargets = Buffer[String]()
    var onOutBufferUpdate:(String => Unit) = _ => None // ServerManagerBot redefines this.

    private def outHandler(): Unit =
        for output <- LazyList.continually(process.stdout.readLine()).takeWhile(_ != null) do
            println(output)
            outBuffer += output
            outBuffer.sliceInPlace(outBuffer.size-outBufferSize, outBufferSize)
            if output.contains("[Server thread/INFO]: Done (") then started = true
            onOutBufferUpdate(output)

        process = null
        started = false
        inBuffer.clear()
        outBuffer.clear()
        startTime = 0

    private def inHandler(): Unit =
        while isRunning do
            val commandOption = inBuffer.headOption
            commandOption match
                case None =>
                case Some(command) =>
                    inBuffer.dropInPlace(1)
                    process.stdin.writeLine(command)
                    process.stdin.flush()
                    if command == "stop" || command == "/stop" then
                        process.wait()
                        println("stopped")

    private def waitForStart() : Unit=
        while !started do
            Thread.sleep(100)

    private def waitForStop() : Unit=
        while isRunning do
            Thread.sleep(100)

    /**
     * Start the server.
     *
     * @param command command for starting the server.
     * @param serverPath path to server directory.
     */
    @throws(classOf[IllegalStateException])
    def start(command: String = Settings.runCommand, serverPath: String = Settings.serverPath): Future[Unit] =
        if !isRunning && !started then
            if !backupRunning then
                startTime = System.currentTimeMillis()
                crashed = false

                process = os.proc(command.split(" ")).spawn(cwd = os.Path(serverPath))

                Future(outHandler())

                val startFuture = Future(waitForStart())
                startFuture.onComplete{
                    case Success(value) =>
                        Future(inHandler())
                    case Failure(exception) =>
                        throw RuntimeException("")
                }

                startFuture
            else
                throw IllegalStateException("A backup operation is running.")
        else
            throw IllegalStateException("The server is already started.")

    /**
     * Stop the server
     */
    @throws(classOf[IllegalStateException])
    def stop(): Future[Unit] =
        if isRunning then
            inBuffer += "stop"

            Future(waitForStop())
        else
            throw IllegalStateException("The server is already stopped.")

    /**
     * Runs the save-all command.
     */
    @throws(classOf[IllegalStateException])
    def save(): Unit =
        if isRunning then
            inBuffer += "save-all"
        else
            throw IllegalStateException("The server is offline, can't save.")

    /**
     * Create a backup while the server is offline.
     *
     * @param worldPath path to the world directory.
     * @param backupPath path to the backup directory.
     * @param tag a tag
     */
    @throws(classOf[IllegalStateException])
    @throws(classOf[FileNotFoundException])
    @throws(classOf[FileAlreadyExistsException])
    def backup(tag: Option[String] = None,
               overwrite: Boolean = false,
               worldPath: String = Settings.worldPath,
               backupPath: String = Settings.backupPath,
               command: String = Settings.compressCommand
              ): (Future[String], String) =

        if !isRunning then
            if os.exists(os.Path(worldPath)) then
                val time = SimpleDateFormat("dd.MM.yyyy-HH.mm.ss").format(System.currentTimeMillis())
                val backupName = s"${os.Path(worldPath).last}-$time" + s"-${tag.mkString.replaceAll("\\W+", "_")}" * tag.size + ".tar.xz"
                val fullPath = os.Path(backupPath) / backupName
                if !os.exists(fullPath) || overwrite then
                    if !backupRunning then
                        if !backupTargets.contains(fullPath.toString) then
                            backupRunning = true
                            backupTargets += fullPath.toString

                            os.makeDir.all(os.Path(backupPath))

                            val backupFuture = Future(command.format(fullPath, worldPath).!!)
                            backupFuture.onComplete(_ =>
                                backupRunning = false
                                backupTargets.remove(backupTargets.indexOf(fullPath.toString)))
                            (backupFuture, backupName)
                        else throw IllegalStateException()
                    else
                        throw IllegalStateException("A backup operation is already running.")
                else
                    throw FileAlreadyExistsException(s"Backup with this name ($fullPath) already exists.")
            else
                throw FileNotFoundException(s"World ($worldPath) not found.")
        else
            throw IllegalStateException("The server is online.")

    /**
     * Load a backup while the server is offline. Deletes the current world.
     *
     * @param backupPath path to the backup file.
     * @param worldPath path to the world directory.
     */
    @throws(classOf[IllegalStateException])
    @throws(classOf[FileNotFoundException])
    def loadBackup(backupPath: String,
                   worldPath: String = Settings.worldPath,
                   command: String = Settings.decompressCommand
                  ): Future[String] =

        if !isRunning then
            if os.exists(os.Path(backupPath)) then
                if !backupRunning then
                    if !backupTargets.contains(backupPath) then
                        backupRunning = true
                        backupTargets += backupPath
                        os.remove.all(os.Path(worldPath))
                        os.makeDir(os.Path(worldPath))
                        val backupFuture = Future(command.format(backupPath, worldPath).!!)
                        backupFuture.onComplete(_ =>
                            backupRunning = false
                            backupTargets.remove(backupTargets.indexOf(backupPath)))
                        backupFuture
                    else
                        throw IllegalStateException("A backup operation is already running.")
                else
                    throw IllegalStateException("A backup operation on this file is already running.")
            else
                throw FileNotFoundException(s"Backup ($backupPath) not found.")
        else
            throw IllegalStateException("The server should not be running.")

    /**
     * Upload a backup to remote storage.
     *
     * @param backupName name of the backup
     * @param backupPath path to the backups directory
     * @param remote rclone path to the remote backups directory
     */
    @throws(classOf[IllegalStateException])
    @throws(classOf[FileNotFoundException])
    @throws(classOf[FileAlreadyExistsException])
    def uploadBackup(backupName: String, backupPath: String = Settings.backupPath, remote: String = Settings.remote, args: String = Settings.args): Future[String] =
        val fullPath = os.Path(backupPath) / backupName
        if os.exists(fullPath) then
            if !s"rclone lsf $remote".!!.contains(backupName) then
                if !backupTargets.contains(fullPath.toString) then
                    backupTargets += fullPath.toString

                    val backupFuture = Future(s"rclone copy $args \"$fullPath\" \"${Settings.remote}\"".!!)
                    backupFuture.onComplete(_ => backupTargets.remove(backupTargets.indexOf(fullPath.toString)))
                    backupFuture
                else
                    throw IllegalStateException("A backup operation on this file is already running.")
            else
                throw FileAlreadyExistsException(s"Backup with this name ($remote/$backupName) already exists.")
        else
            throw FileNotFoundException(s"Backup ($fullPath) not found.")

    /**
     * Download a backup from remote storage.
     *
     * @param backupName name of the backup
     * @param backupPath path to the backups directory
     * @param remote rclone path to the remote backups directory
     */
    @throws(classOf[IllegalStateException])
    @throws(classOf[FileNotFoundException])
    @throws(classOf[FileAlreadyExistsException])
    def downloadBackup(backupName: String, backupPath: String = Settings.backupPath, remote: String = Settings.remote, args: String = Settings.args): Future[String] =
        val fullPath = os.Path(backupPath) / backupName
        if !os.exists(fullPath) then
            if s"rclone lsf $remote".!!.contains(backupName) then
                if !backupTargets.contains(fullPath.toString) then
                    backupTargets += fullPath.toString

                    val backupFuture = Future(s"rclone copy $args \"$remote/$backupName\" \"$backupPath\"".!!)
                    backupFuture.onComplete(_ => backupTargets.remove(backupTargets.indexOf(fullPath.toString)))
                    backupFuture
                else
                    throw IllegalStateException("A backup operation on this file is already running.")
            else
                throw FileNotFoundException(s"Backup ($remote/$backupName) not found.")
        else
            throw FileAlreadyExistsException(s"Backup with this name ($fullPath) already exists.")

    /**
     * List of all local and remote backups sorted by creation time
     *
     * @param backupPath path to the backups directory
     * @param remote rclone path to the remote backups directory
     * @return formatted string containing the backups
     */
    def listBackups(backupPath: String = Settings.backupPath, remote: String = Settings.remote): String =
        val format = SimpleDateFormat("dd.MM.yyyy-HH.mm.ss")

        s"<b>Local backups (${Seq("bash", "-c", Settings.localStorageUseCommand.replaceFirst("%s", Settings.backupPath)).!!.trim}):</b>\n"
            + os.list(os.Path(backupPath)).sortBy(f => format.parse(f.last.split('-').slice(1, 3).mkString("-"))).map("\t<code>" + _.last + "</code>").mkString("\n")
            + {if remote.nonEmpty then
                   s"\n<b>Remote backups (${Seq("bash", "-c", Settings.remoteStorageUseCommand.replaceFirst("%s", Settings.remote)).!!.trim}):</b>\n"
                       + s"rclone lsf $remote".!!.split('\n').sortBy(f => format.parse(f.split('-').slice(1, 3).mkString("-"))).map("\t<code>" + _ + "</code>").mkString("\n")
               else ""}

    /**
     * Delete a local backup.
     *
     * @param backupPath path to the backup to be deleted
     */
    @throws(classOf[IllegalStateException])
    @throws(classOf[FileNotFoundException])
    def deleteBackup(backupPath: String): Unit =
        if os.exists(os.Path(backupPath)) then
            if !backupTargets.contains(backupPath) then
                os.remove(os.Path(backupPath))
            else
                throw IllegalStateException("A backup operation on this file is already running.")
        else
            throw FileNotFoundException(s"Backup ($backupPath) not found.")

    /**
     * Delete a remote backup.
     *
     * @param backupName path to the backup to be deleted
     * @param backupPath path to local backup directory
     * @param remote rclone path to the remote backups directory
     */
    @throws(classOf[IllegalStateException])
    @throws(classOf[FileNotFoundException])
    def deleteRemoteBackup(backupName: String, backupPath: String = Settings.backupPath, remote: String = Settings.remote): Unit =
        if s"rclone lsf $remote".!!.contains(backupName) then
            if !backupTargets.contains((os.Path(backupPath) / backupName).toString) then
                s"rclone delete $remote/$backupName".!!
            else
                throw IllegalStateException("A backup operation on this file is already running.")
        else
            throw FileNotFoundException(s"Backup ($remote/$backupName) not found.")

    /**
     * Run a command.
     *
     * @param command the command
     */
    @throws(classOf[IllegalStateException])
    @throws(classOf[IllegalArgumentException])
    def runCommand(command: String): Unit =
        if isRunning then
            if command != "stop" && command != "/stop" then
                inBuffer += command
            else
                throw IllegalArgumentException("The server must be stopped by using the stop command.")
        else
            throw IllegalStateException("The server is offline.")

    /**
     * Get uptime.
     *
     * @return A human readable string.
     */
    @throws(classOf[IllegalStateException])
    def uptime: String =
        if isRunning then
            val secDiff = ((System.currentTimeMillis() - startTime) / 1000).toInt
            val (days, remainder) = secDiff /% 86400
            val (hours, remainder2) = remainder /% 3600
            val (minutes, seconds) = remainder2 /% 60

            val result = Seq(
                s"$days days",
                s"$hours hours",
                s"$minutes minutes",
                s"$seconds seconds"
            ).filter(_.head != '0').mkString(", ")

            if result.contains(',') then
                result.patch(result.lastIndexOf(","), " and", 1)
            else result
        else
            throw IllegalStateException("The server is offline.")

    /**
     * Is the server online?
     *
     * @return result
     */
    def isRunning: Boolean =
        if process == null || !process.isAlive() then
            false
        else
            true

    /**
     * Get a object with informtion from server list ping
     *
     * @param port server port, default is 25565
     * @return MineStat object that also includes playerList
     */
    @throws(classOf[IllegalStateException])
    def serverListPing(port: Int = Settings.port): MineStatModified =
        if isRunning then
            MineStatModified("127.0.0.1", port)
        else
            throw IllegalStateException("The server is offline.")

    /**
     * Get a string containing information about the status of the server
     *
     * @return status string
     */
    def getStatus: String =
        {
            if isRunning then
                val serverStatus = serverListPing()
                "Server is currently online\n"
                    + s"Uptime: $uptime\n"
                    + {if !serverStatus.getPlayerList.isEmpty then
                        s"Players (${serverStatus.getCurrentPlayers}/${serverStatus.getMaximumPlayers}): ${String.join((", "), serverStatus.getPlayerList)}\n"
                    else
                        s"No players online (0/${serverStatus.getMaximumPlayers})\n"}
            else
                "Server is currently offline\n"
        }
            + s"Load: ${Seq("bash", "-c", Settings.loadCommand).!!}"
            + s"Temperature:\n${Seq("bash", "-c", Settings.tempCommand).!!}"
            + s"RAM:\n${Seq("bash", "-c", Settings.ramCommand).!!}"
