package s1.telegrambots
import com.bot4s.telegram.methods.{ParseMode, SendMessage}
import com.bot4s.telegram.models.ChatId

import java.io.FileNotFoundException
import java.nio.charset.StandardCharsets
import java.nio.file.FileAlreadyExistsException
import scala.jdk.CollectionConverters.*
import scala.util.{Failure, Success, Using}
import concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.io.Source
import scala.sys.process.*

object ServerManagerBot extends App:

    val USER_NOT_ADMIN = "This command can only be used by admins."

    object Bot extends BasicBot:
        val api = API(1000)

        /**
         * Check if sender is a approved admin.
         *
         * @param msg message to verify sender for
         * @return Is the user a valid admin
         */
        def checkUser(msg: Message): Boolean =
            msg.from match
                case None => false
                case Some(sender) => Settings.adminUserIDs.contains(sender.id)

        // ------------------------------------------- Public commands -------------------------------------------

        /**
         * Should return domain if defined else public ip
         * Should include port if not standard (25565) and srv is enabled and domain is defined
         *
         * Format:
         * [Domain/IP]:[Port] or [Domain/IP]
         *
         * @param msg the message invoking this
         */
        def ip(msg: Message): String =
            println("ip")
            {
                if Settings.domain.nonEmpty then
                    s"<code>${Settings.domain}"
                else
                    val src = scala.io.Source.fromURL("https://api.ipify.org")
                    val ip = src.mkString
                    src.close()
                    s"<code>$ip"
            }
                + (if (Settings.port != 25565 && !Settings.srv && Settings.domain.nonEmpty) ||
                      (Settings.port != 25565 && Settings.srv && Settings.domain.isEmpty) ||
                      (Settings.port != 25565 && !Settings.srv && Settings.domain.isEmpty) then s":${Settings.port}</code>" else "</code>")


        /**
         * Current status of the server.
         *
         * @param msg the message invoking this
         */
        def status(msg: Message): String =
            println("status")
            api.getStatus

        /**
         * Use to get your telegram id
         *
         * @param msg the message invoking this
         */
        def identify(msg: Message): String =
            println("id")
            val userString=
                (msg.from match
                case None => "No sender"
                case Some(user) => "User ID of " + user.username.getOrElse("nameless user") +":\n" + s"<code>${user.id.toString}</code>")

            val chatString= "\n\nChat ID of " + msg.chat.username.getOrElse("nameless chat") +":\n" +  s"<code>${msg.chat.id.toString}</code>"
            userString + chatString

        /**
         * Get a help message that tells you how to use the commands.
         *
         * @param msg the message invoking this
         */
        def help(msg: Message): String =
            println("help")
            val source = Source.fromFile("help.md")

            try
                source.getLines.mkString("\n")
            catch
                case e: Exception =>
                    e.printStackTrace()
                    "An error occured while trying to read help.md"
            finally
                source.close()
                println("closed")

        // ------------------------------------------- Admin commands -------------------------------------------
        // These should verify that the user is a admin before doing anything.

        /**
         * Verify the server  isn't running, then start it.
         *
         * @param msg the message invoking this
         */
        def start(msg: Message): String =
            println("st")
            if !checkUser(msg) then return USER_NOT_ADMIN

            try
                val startFuture = api.start()
                startFuture.onComplete{
                    case Success(value) => writeMessage("Server started.", msg.chat.chatId)
                    case Failure(e) => writeMessage(s"Starting server failed: $e", msg.chat.chatId)
                }
                "Starting server..."
            catch
                case e : IllegalStateException => "Error: " + e.getMessage
                case e: Exception => "Error: " + e.toString

        /**
         * Verify the server  is running, then stop it.
         *
         * @param msg the message invoking this
         */
        def stop(msg: Message): String =
            println("sp")
            if !checkUser(msg) then return USER_NOT_ADMIN

            try
                val stopFuture = api.stop()
                stopFuture.onComplete{
                    case Success(value) => writeMessage("Server stopped.", msg.chat.chatId)
                    case Failure(e) => writeMessage(s"Stopping server failed: $e", msg.chat.chatId)
                }
                "Stopping server..."

            catch
                case e:IllegalStateException => "Error: " + e.getMessage
                case e: Exception => "Error: " + e.toString

        /**
         * Verify the server is running, then save.
         *
         * @param msg the message invoking this
         */
        def save(msg: Message):String =
            println("sv")
            if !checkUser(msg) then return USER_NOT_ADMIN

            try
              api.save()
              "Saving..."
            catch
                case e:IllegalStateException => "Error: " + e.getMessage
                case e: Exception => "Error: " + e.toString

        // Backup commands. The old syntax if horrible so its split into multiple commands.
        /**
         * Create a local backup.
         *
         * /backup [optional tag]
         *
         * @param msg the message invoking this
         */
        def backup(msg: Message): String =
            println("cb")
            if !checkUser(msg) then return USER_NOT_ADMIN

            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            try
                val (backupFuture, _) = api.backup(args.lift(1))
                backupFuture.onComplete{
                    case Success(value) => writeMessage("Backup created.", msg.chat.chatId)
                    case Failure(e) => writeMessage(s"Failed to create backup: $e", msg.chat.chatId)
                }
                "Creating backup..."
            catch
                case e: IllegalStateException => "Error: " + e.getMessage
                case e: FileNotFoundException => "Error: " + e.getMessage
                case e: FileAlreadyExistsException => "Error: " + e.getMessage
                case e: Exception => "Error: " + e.toString

        /**
         * Create a local backup and start the server.
         *
         * /backupStart [optional tag]
         *
         * @param msg the message invoking this
         */
        def backupStart(msg: Message): String =
            println("bs")
            if !checkUser(msg) then return USER_NOT_ADMIN

            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            try
                val (backupFuture, _) = api.backup(args.lift(1))
                backupFuture.onComplete{
                    case Success(value) =>
                        writeMessage("Backup created. Starting server...", msg.chat.chatId)

                        val startFuture = api.start()
                        startFuture.onComplete{
                            case Success(value) => writeMessage("Server started.", msg.chat.chatId)
                            case Failure(e) => writeMessage(s"Starting server failed: $e", msg.chat.chatId)
                        }
                    case Failure(e) => writeMessage(s"Failed to create backup: $e", msg.chat.chatId)
                }
                "Creating backup..."
            catch
                case e: IllegalStateException => "Error: " + e.getMessage
                case e: FileNotFoundException => "Error: " + e.getMessage
                case e: FileAlreadyExistsException => "Error: " + e.getMessage
                case e: Exception => "Error: " + e.toString

        /**
         * Create a local backup and upload it.
         *
         * /backupUpload [optional tag]
         *
         * @param msg the message invoking this
         */
        def backupUpload(msg: Message): String =
            println("bu")
            if !checkUser(msg) then return USER_NOT_ADMIN
            if Settings.remote.isEmpty then return s"rclone not configured. \n${backup(msg)}"

            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            try
                val (backupFuture, backupName) = api.backup(args.lift(1))
                backupFuture.onComplete{
                    case Success(value) =>
                        writeMessage("Backup created. Uploading backup...", msg.chat.chatId)

                        val uploadFuture = api.uploadBackup(backupName)
                        uploadFuture.onComplete{
                            case Success(value) => writeMessage(s"$backupName uploaded.", msg.chat.chatId)
                            case Failure(e) => writeMessage(s"Failed to upload $backupName: $e", msg.chat.chatId)
                        }
                    case Failure(e) => writeMessage(s"Failed to create backup: $e", msg.chat.chatId)
                }
                "Creating backup..."
            catch
                case e: IllegalStateException => "Error: " + e.getMessage
                case e: FileNotFoundException => "Error: " + e.getMessage
                case e: FileAlreadyExistsException => "Error: " + e.getMessage
                case e: Exception => "Error: " + e.toString

        /**
         * Create a local backup, start the server and upload the backup.
         *
         * /backupStartUpload [optional tag]
         *
         * @param msg the message invoking this
         */
        def backupStartUpload(msg: Message): String =
            println("bsu")
            if !checkUser(msg) then return USER_NOT_ADMIN
            if Settings.remote.isEmpty then return s"rclone not configured. \n${backupStart(msg)}"

            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            try
                val (backupFuture, backupName) = api.backup(args.lift(1))
                backupFuture.onComplete{
                    case Success(value) =>
                        writeMessage("Backup created. Starting server and uploading backup...", msg.chat.chatId)

                        val startFuture = api.start()
                        startFuture.onComplete{
                            case Success(value) => writeMessage("Server started.", msg.chat.chatId)
                            case Failure(e) => writeMessage(s"Starting server failed: $e", msg.chat.chatId)
                        }

                        val uploadFuture = api.uploadBackup(backupName)
                        uploadFuture.onComplete{
                            case Success(value) => writeMessage(s"$backupName uploaded.", msg.chat.chatId)
                            case Failure(e) => writeMessage(s"Failed to upload $backupName: $e", msg.chat.chatId)
                        }
                    case Failure(e) => writeMessage(s"Failed to create backup: $e", msg.chat.chatId)
                }
                "Creating backup..."
            catch
                case e: IllegalStateException => "Error: " + e.getMessage
                case e: FileNotFoundException => "Error: " + e.getMessage
                case e: FileAlreadyExistsException => "Error: " + e.getMessage
                case e: Exception => "Error: " + e.toString

        /**
         * Load a local backup.
         *
         * /loadBackup [local filename]
         *
         * @param msg the message invoking this
         */
        def loadBackup(msg: Message): String =
            println("lb")
            if !checkUser(msg) then return USER_NOT_ADMIN

            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            args.lift(1) match
                case Some(target) =>
                    try
                        val backupFuture = api.loadBackup((os.Path(Settings.backupPath) / target).toString)
                        backupFuture.onComplete{
                            case Success(value) => writeMessage("Backup loaded.", msg.chat.chatId)
                            case Failure(e) => writeMessage(s"Failed to load backup: $e", msg.chat.chatId)
                        }
                        "Loading backup..."
                    catch
                        case e: IllegalStateException => "Error: " + e.getMessage
                        case e: FileNotFoundException => "Error: " + e.getMessage
                        case e: Exception => "Error: " + e.toString

                case None => "No target specified."

        /**
         * Load a local backup and start the server.
         *
         * /loadStart [local filename]
         *
         * @param msg the message invoking this
         */
        def loadStart(msg: Message): String =
            println("lbs")
            if !checkUser(msg) then return USER_NOT_ADMIN

            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            args.lift(1) match
                case Some(target) =>
                    try
                        val loadFuture = api.loadBackup((os.Path(Settings.backupPath) / target).toString)
                        loadFuture.onComplete{
                            case Success(value) =>
                                writeMessage("Backup loaded. Starting server...", msg.chat.chatId)

                                val startFuture = api.start()
                                startFuture.onComplete{
                                    case Success(value) => writeMessage("Server started.", msg.chat.chatId)
                                    case Failure(e) => writeMessage(s"Starting server failed: $e", msg.chat.chatId)
                                }
                            case Failure(e) => writeMessage(s"Failed to load backup: $e", msg.chat.chatId)
                        }


                        "Loading backup..."
                    catch
                        case e: IllegalStateException => "Error: " + e.getMessage
                        case e: FileNotFoundException => "Error: " + e.getMessage
                        case e: Exception => "Error: " + e.toString

                case None => "No target specified."

        /**
         * Delete a local backup.
         *
         * /deleteBackup [local filename]
         *
         * @param msg the message invoking this
         */
        def deleteBackup(msg: Message): String =
            println("rl")
            if !checkUser(msg) then return USER_NOT_ADMIN

            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            args.lift(1) match
                case Some(target) =>
                    try
                        api.deleteBackup((os.Path(Settings.backupPath) / target).toString)
                        "Backup removed."
                    catch
                        case e: IllegalStateException => "Error: " + e.getMessage
                        case e: FileNotFoundException => "Error: " + e.getMessage
                        case e: Exception => "Error: " + e.toString

                case None => "No target specified."

        /**
         * Delete a remote backup.
         *
         * /deleteBackup [remote filename]
         *
         * @param msg the message invoking this
         */
        def deleteRemoteBackup(msg: Message): String =
            println("rr")
            if !checkUser(msg) then return USER_NOT_ADMIN

            if Settings.remote.isEmpty then return "rclone not configured."
            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            args.lift(1) match
                case Some(target) =>
                    try
                        api.deleteRemoteBackup(target)
                        "Backup removed."
                    catch
                        case e: IllegalStateException => "Error: " + e.getMessage
                        case e: FileNotFoundException => "Error: " + e.getMessage
                        case e: Exception => "Error: " + e.toString

                case None => "No target specified."

        /**
         * Get a list of local and remote backups. Includes disk usage.
         *
         * @param msg the message invoking this
         */
        def listBackups(msg: Message): String =
            println("ls")
            if !checkUser(msg) then return USER_NOT_ADMIN

            val list = api.listBackups()

            if list.length <= 4096 then
                list
            else
                request(com.bot4s.telegram.methods.SendDocument(msg.source, com.bot4s.telegram.models.InputFile("backups.txt", list.getBytes(StandardCharsets.UTF_8))))
                "List too long, here is a file instead."

        /**
         * Upload a backup.
         *
         * /updloadBackup [local filename]
         *
         * @param msg the message invoking this
         */
        def updloadBackup(msg: Message): String =
            println("ul")
            if !checkUser(msg) then return USER_NOT_ADMIN

            if Settings.remote.isEmpty then return "rclone not configured."
            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            args.lift(1) match
                case Some(target) =>
                    try
                        val backupFuture = api.uploadBackup(target)
                        backupFuture.onComplete{
                            case Success(value) => writeMessage(s"$target uploaded.", msg.chat.chatId)
                            case Failure(e) => writeMessage(s"Failed to upload $target: $e", msg.chat.chatId)
                        }
                        "Uploading backup..."
                    catch
                        case e: IllegalStateException => "Error: " + e.getMessage
                        case e: FileNotFoundException => "Error: " + e.getMessage
                        case e: FileAlreadyExistsException => "Error: " + e.getMessage
                        case e: Exception => "Error: " + e.toString

                case None => "No target specified."

        /**
         * Download a backup
         *
         * /downloadBackup [remote filename]
         *
         * @param msg the message invoking this
         */
        def downloadBackup(msg: Message): String =
            println("dl")
            if !checkUser(msg) then return USER_NOT_ADMIN

            if Settings.remote.isEmpty then return "rclone not configured."
            val args = msg.text.get.split(" ", 2)  // text can't be empty since it must at least include the command
            args.lift(1) match
                case Some(target) =>
                    try
                        val backupFuture = api.downloadBackup(target)
                        backupFuture.onComplete{
                            case Success(value) => writeMessage(s"$target downloaded.", msg.chat.chatId)
                            case Failure(e) => writeMessage(s"Failed to download $target: $e", msg.chat.chatId)
                        }
                        "Downloading backup..."
                    catch
                        case e: IllegalStateException => "Error: " + e.getMessage
                        case e: FileNotFoundException => "Error: " + e.getMessage
                        case e: FileAlreadyExistsException => "Error: " + e.getMessage
                        case e: Exception => "Error: " + e.toString

                case None => "No target specified."

        /**
         * Replies with the recent terminal log.
         *
         * @param msg the message invoking this
         */
        def getLog(msg: Message):String =
            println("log")
            if !checkUser(msg) then return USER_NOT_ADMIN
            if api.outBuffer.isEmpty then return "The log is empty."

            val args = msg.text.get.split(" ",2)

            val log = if args.length == 2 then
                val lines = args(1).toIntOption
                if lines.isEmpty then
                    return "Argument is not a number."
                else
                    api.outBuffer.take(lines.get).mkString("\n")
            else
                api.outBuffer.mkString("\n")

            if log.length <= 4096 then
                log
            else
                request(com.bot4s.telegram.methods.SendDocument(msg.source, com.bot4s.telegram.models.InputFile("log.txt", log.getBytes(StandardCharsets.UTF_8))))
                "The log is too long, here is a file instead."

        /**
         * Run a command. Considers everything after the first space the command to be run.
         *
         * @param msg the message invoking this
         */
        def runCommand(msg: Message): String =
            println("exec")
            if !checkUser(msg) then return USER_NOT_ADMIN

            val args = msg.text.get.split(" ",2)  // text can't be empty since it must at least include the command

            args.lift(1) match
                case Some(command) =>
                    try
                        api.runCommand(command)
                        "Ran the command."
                    catch
                        case e: IllegalStateException => "Error: " + e.getMessage
                        case e: IllegalArgumentException => "Error: " + e.getMessage
                        case e: Exception => "Error: " + e.toString
                case None =>
                    "No command specified."

        /**
         * run a game command that prints to the game chat
         * AdminTelegrammer: /say hello everyone
         * prints to game chat: Telegram admin @AdminTelegrammer: hello everyone
         * (Not implemented: hovering over the game chat message says "sent through telegram server manager bot)"
         *
         * @param msg the message invoking this
         */
        def say(msg: Message):String =
            println("say")
            if !checkUser(msg) then return USER_NOT_ADMIN

            val args = msg.text.get.split(" ",2)  // text can't be empty since it must at least include the command

            args.lift(1) match
                case Some(message) =>
                    val username = msg.from.get.username.getOrElse("NONE")
                    try
                        api.runCommand(s"say Telegram admin @$username: $message")
                        "Said in chat."
                    catch
                        case e: IllegalStateException => "Error: " + e.getMessage
                        case e: Exception => "Error: " + e.toString
                case None =>
                    "Nothing to say?"

        /**
         * Send new log output to subscribed channels
         *
         * @param log the new line in the log
         */
        def onLogUpdate(log: String):Unit =
            if api.started then Settings.logBroadcastSubscriberChatIDs.stream().forEach(id => writeMessage(s"<code>$log</code>", ChatId(id)))

        api.onOutBufferUpdate = onLogUpdate

        // Register commands
        this.onHelp(help)

        this.onUserCommand("status", status)

        this.onUserCommand("address", ip)
        this.onUserCommand("ip", ip)

        this.onUserCommand("identify", identify)
        this.onUserCommand("id", identify)

        this.onUserCommand("startServer", start)
        this.onUserCommand("st", start)

        this.onUserCommand("stopServer", stop)
        this.onUserCommand("sp", stop)

        this.onUserCommand("save", save)
        this.onUserCommand("sv", save)

        this.onUserCommand("run", runCommand)
        this.onUserCommand("command", runCommand)
        this.onUserCommand("exec", runCommand)

        this.onUserCommand("getLog", getLog)
        this.onUserCommand("log", getLog)

        this.onUserCommand("say", say)

        this.onUserCommand("createBackup", backup)
        this.onUserCommand("backup", backup)
        this.onUserCommand("cb", backup)

        this.onUserCommand("backupStart", backupStart)
        this.onUserCommand("bs", backupStart)

        this.onUserCommand("backupUpload", backupUpload)
        this.onUserCommand("bu", backupUpload)

        this.onUserCommand("backupStartUpload", backupStartUpload)
        this.onUserCommand("bsu",  backupStartUpload)

        this.onUserCommand("loadBackup", loadBackup)
        this.onUserCommand("load", loadBackup)
        this.onUserCommand("lb", loadBackup)

        this.onUserCommand("loadStart", loadStart)
        this.onUserCommand("lbs", loadStart)

        this.onUserCommand("listBackups", listBackups)
        this.onUserCommand("ls", listBackups)

        this.onUserCommand("removeLocal", deleteBackup)
        this.onUserCommand("rl", deleteBackup)

        this.onUserCommand("removeRemote", deleteRemoteBackup)
        this.onUserCommand("rr", deleteRemoteBackup)

        this.onUserCommand("downloadBackup", downloadBackup)
        this.onUserCommand("download", downloadBackup)
        this.onUserCommand("dl", downloadBackup)

        this.onUserCommand("uploadBackup", updloadBackup)
        this.onUserCommand("upload", updloadBackup)
        this.onUserCommand("ul", updloadBackup)

        val botFuture = this.run()

    val bot = Bot

    println("started")

    // Stop the program from exiting
    Await.ready(bot.botFuture, Duration.Inf)
